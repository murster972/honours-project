use NetFlowClassification

db.createCollection("Collectors")
db.createCollection("NetFlow_data")
db.createCollection("thresholds")
db.createCollection("activity_log")
db.createCollection("settings")

db.Collectors.drop()

db.Collectors.insert({
	"name": "Router-A",
	"ip": "127.0.0.1",
	"port": 2090,
	"status": 1,
	"flows": [],
	"activity_log": []
})

db.Collectors.insert({
	"name": "Router-B",
	"ip": "127.0.0.1",
	"port": 2080,
	"status": 0,
	"flows": [],
	"activity_log": []
})

// db.NetflowData.insert({
// 	"_id": 1290912,
// 	"src": 101010101,
// 	"records": {0: [(8, 167772162), (12, 167772163), (15, 0), (10, 3), (14, 5), (2, 1), (1, 64), (7, 4242), (11, 80), (6, 0), (4, 17), (5, 1), (17, 3), (16, 2), (9, 32), (13, 31), (21, 30210580), (22, 30150580)]}
// })

show collections

db.Collectors.find()
// db.NetflowData.find()
