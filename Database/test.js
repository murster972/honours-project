use NetFlowClassification

db.Thresholds.insert({
	"name": "Router-B",
    "collectors": ["5cb65862edfe29a16b57e813"],
	"src-ip": "192.169.0.1",
    "dest-ip": "127.0.0.1",
    "src-mac": "",
    "dest-mac": "",
    "src-port": "",
    "dest-port": "",
    "src-vlan": "",
    "dest-vlan": "",
    "packet-count": "",
    "max-bytes": "",
    "direction": "",
    "next-hop": "",
})
