this dir contains the raw bytes of captured flow export records.
this has been done so the parser can be tested outwith the lab.

each folder represents one session of capture, with the files inside.

containning the raw bytes they each have a number at the start and should be
read in ascending order as to replicate the order they were recieved.

currently flows have been generated with pings (icmp), http and telnet
