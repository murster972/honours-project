# configure
enable
conf t
# conf export address, 2 max
ip flow-export dest IP_ADDR UDP_PORT
# conf ver
ip flow-export ver 9

# conf on interface (req)
int fa0/0
ip flow ingress/egress

# conf templates

# set timers

# verify
show ip flow interface
show ip cache flow
