#!/usr/bin/env python3
from modules import database_handler
from modules.misc.validation import valid_ip, valid_port, valid_name

''' Functionaility for flask app.py '''

db_handler_threshold = database_handler.Thresholds()
db_hander_collector = database_handler.Collectors()
db_handler_flows = database_handler.Flows()

# ========== NetFlow page ==============
def get_collector_info(minimum=False, collector_id=None):
    ' gets and returns collector/s '
    if collector_id:
        return db_hander_collector.get_collectors(collector_id=collector_id)
    else:
        info = db_hander_collector.get_collectors()

        if minimum:
            # return only collector id, name and status
            info = [{"id": c["_id"], "name": c["name"], "status": c["status"]} for c in info]

        return info

def get_collector_flow_stats(collector_id):
    ' gets flow stats for a collector '
    # number of Flows

    # incoming rate

    # storage used

    # arhieve flows

def get_collector_activity_log(collector_id):
    ' gets activities for collector '
    pass

def delete_collector(collector_id):
    ''' deletes collector
        :return int: number of collectors deleted '''
    # TODO: check if collector exists first
    return db_hander_collector.remove_collector(collector_id)

def get_netflow_data_stats():
    ' gets stored flows and generates their stats '
    pass

def add_collector(data):
    ' adds collector '
    # check if data is valid
    values = {"name": "", "ip": "", "port": ""}
    validation = {"name": lambda n: valid_name(n),
                  "ip": lambda ip: valid_ip(ip),
                  "port": lambda p: valid_port(p)}

    invalid = []

    # checks if name, ip and port exsist and are valid
    # if so addes to values dict, creates values so that any additoanl
    # values passed are dropped and not passed to dbHandler
    for i in validation.keys():
        if i not in data or not validation[i](data[i]):
            invalid.append(i)
        values[i] = data[i]

    if len(invalid): return ("1", ", ".join(invalid))

    return db_hander_collector.add_collector(values)

def modify_collector(id, modified_values):
    ' modify collector values '
    pass

# ========== Analysis page ==============
def get_thresholds():
    info = db_handler_threshold.get_thresholds()

    return info

def create_threshold(data):
    # apply validation

    return db_handler_threshold.add_threshold(data)

def delete_threshold(_id):
    return db_handler_threshold.delete_threshold(_id)

def get_flows():
    return db_handler_flows.get_flows()

if __name__ == '__main__':
    x = get_flows()
    print(x)
