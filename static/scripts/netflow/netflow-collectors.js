var temp;

class NetflowCollectors {
    /* collection of functions for loading/creating/removing collectors.
       the functions are the callbacks from ajax post/get to server */
    constructor(){
        this.selectedCollectorID = null;
    }

    load_collector_info(data){
        /* loads netflow collector info into  UI */
        // update general-info
        let info = JSON.parse(data)

        $("#info_name").val(info["name"])
        $("#info_ip").val(info["ip"])
        $("#info_port").val(info["port"])

        if(info["status"] == "active"){
            $("#info_status").text("ACTIVE").removeClass("inactive").addClass("active");
        } else{
            $("#info_status").text("INACTIVE").removeClass("active").addClass("inactive");
        }

        // update flow info
        $("#flow_number").val(info["flows"].length)

        // update activity-log
        let new_entry = function(title, msg){
            return '<div class="activity">\
                    <h3>[' + title +']</h3>\
                    <p>[' + msg + ']</p>\
                    </div>'
        }

        $("#log-container").html("")

        for(let i = 0; i < info["activity_log"].length; i++){
            let a = info["activity_log"][i];
            let e = new_entry(a["title"], a["msg"]);
            $("#log-container").append(e);
        }
    }

    updater(){
        temp = setInterval(function(){
            $.post("/netflow_collector", function(data){
                let collectors = JSON.parse(data);

                let new_data = {};

                for(let i = 0; i < collectors.length; i++){
                    let c = collectors[i];

                    let id = c["_id"];
                    let name = c["name"];
                    let status = c["status"];
                    let flow_rate = c["flow-rate"];

                    // update graph
                    new_data[name] = flow_rate;
                    //new_data[name] = {time: tempCounter, value: 2}

                    // update status
                    if(status == "active"){
                        $("#" + id + " .status-icon").removeClass("inactive").addClass("active")
                    } else{
                        $("#" + id + " .status-icon").removeClass("active").addClass("inactive")
                    }
                }

                //console.log(new_data)

                flowGraph.updateValues(new_data);
                //console.log(new_data)
            });

            //clearInterval(temp);
        }, 1000);
    }

    update_status(){
        /* ran on timer to check for update */
        setInterval(function(){
            $.post("/netflow_collector", function(data){
                let collectors = JSON.parse(data)

                collectors.forEach(function(c){
                    let id = c["_id"];
                    let status = c["status"];

                    if(status == "active"){
                        $("#" + id + " .status-icon").removeClass("inactive").addClass("active")
                    } else{
                        $("#" + id + " .status-icon").removeClass("active").addClass("inactive")
                    }
                })
            });
        }, 5000);
    }

    create_collector(resp){
        /* creates new netflow collector */
        let status_msg = JSON.parse(resp)

        if(status_msg["status"] == "0"){
            alert("new collector added!")

            // refresh page to show new collector
            location.href = location.href
        }

        else if(status_msg["status"] == "2"){
            alert("Collector not added, duplicate values exsist for: " + status_msg["msg"])
        }

        else if(status_msg["status"] == "1"){
            alert("Collector not added, invalid-data for: " + status_msg["msg"])
        }

        // add to collector-list

        // add to graph

    }

    delete_collector(resp){
        /* deltes netflow collector */
        if(resp == "1"){
            alert("collector deleted.")

            // get name
            let name = $("#" + netflow.selectedCollectorID + " > " + ".collector-name").text();

            // remove from list
            //NOTE: its netflow. and not this. since its called from ajax function
            //      this is set to the ajax function
            $("#" + netflow.selectedCollectorID).remove();

            // remove from id list
            let ind = collectorIds.indexOf(netflow.selectedCollectorID);
            collectorIds.splice(ind, 1);

            // remove from graph
            let index = flowGraph.dataPoints.findIndex(c => c.name == name);
            console.log(flowGraph.dataPoints[index])
            flowGraph.dataPoints.splice(0, index);
            flowGraph.chart.render();

            flowGraph.selectedCollectorID = null;

        } else if(resp == "2"){
            alert("No Collector selected.")
        } else{
            alert("Unable to delete collector.")
        }
    }
}
