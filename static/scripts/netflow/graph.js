/* IncomingFlows is used to represent the graph that displays incoming
 * collector flows.
 */


// TODO: highlight series in legend hover, https://canvasjs.com/forums/topic/legend-highlight/

class IncomingFlows {
    constructor(collector_names){
        this.container_id = "incoming-flow-chart";

        this.collector_names = collector_names;

        // stores collectors in dict so easier to update
        // i.e. can simply go through by key instead of having
        // to search whole array for right collector name
        this.collectors = {};

        // list of dataPoints, each element refers to a "dataPoints" key from
        // a collector entry in this.collectors, as the "dataPoints" are updated inspect
        // collectors the changes are reflected here
        // canvasjs requires data to be array where each item is a "series" hence
        // the array
        this.dataPoints = [];

        // numeber of points in graph
        this.pointCounter = 0;
        this.maxPoints = 20;

        for(let i = 0; i < collector_names.length; i++){
            let name = collector_names[i];

            // options to display collector on graph
            this.collectors[name] = {
                type: "splineArea",
                axisYType: "secondary",
                name: name,
                showInLegend: true,
                markerSize: 5,
                xValueFormatString: "H:m:s",
                highlightEnabled: true,
                dataPoints: []
            };

            this.dataPoints.push(this.collectors[name]);
        }

        this.chart = new CanvasJS.Chart(this.container_id,{
            title: {
                text: "Number of Incoming Flows"
            }, axisX: {
                title: "Time (s)",
                valueFormatString: "H:m:s"
        	}, axisY2: {
        		title: "No. of Flows",
        	}, legend: {
        		cursor: "pointer",
        		verticalAlign: "top",
        		horizontalAlign: "left",
        		dockInsidePlotArea: false,
        	},
            data: this.dataPoints
        })

        this.chart.render();
    }

    /*
     * updates each collector in the graph with a new value
     *
     * @param {object} data, the data to update, takes format {name1: value, ...} */
    updateValues(data){
        for(let i = 0; i < this.collector_names.length; i++){
            let name = this.collector_names[i];
            let new_data = data[name];

            // limits max number of points that can in graph
            // does this for two reasons: 1 - it stops the data getting to crammped on the graph,
            //                            2 - it stops the size of the arrays storing the data from
            //                                becoming very large.
            if(this.pointCounter >= this.maxPoints){
                this.collectors[name].dataPoints.shift();
            }

            //console.log(new_data.value)
            
            // BUG: incorrect time
            this.collectors[name].dataPoints.push({x: new Date(new_data["time"]), y: new_data["no"]});

            //console.log(this.collectors[name].dataPoints)
            var milliseconds = (new Date).getTime();
            //console.log(new_data["time"])

            //this.collectors[name].dataPoints.push({x: new Date(milliseconds), y: Math.floor((Math.random() * 20) + 1)});
        }

        this.pointCounter++;

        this.chart.render();
    }
}
