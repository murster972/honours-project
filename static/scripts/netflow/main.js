var netflow, flowGraph;

var tempCounter = 0;

$(document).ready(function(){
    flowGraph = new IncomingFlows(collectorNames);
    netflow = new NetflowCollectors();

    netflow.updater();

    $("#add-device").click(function(){
        $(".new-device-popup").removeClass("hideMe")
    })

    $("#add-device-popup #content #close").click(function(){
        $(".new-device-popup").addClass("hideMe")
    })

    $("#add-device-popup #content #add").click(function(){
        let parent = "#add-device-popup #content ";
        let name = $(parent + "#name").val();
        let ip = $(parent + "#ip").val();
        let port = $(parent + "#port").val();
        let data = {name: name, ip: ip, port: port}

        $.post("/add_collector", data, netflow.create_collector);
    })

    $(".more-info").click(function(){
        let collectorID = $(this).attr("collector_id");
        netflow.selectedCollectorID = collectorID;

        $.post("/netflow_collector", collectorID, netflow.load_collector_info);
    })

    $("#delete-collector").click(function(){
        $.post("/delete_collector", netflow.selectedCollectorID, netflow.delete_collector);
    })

    $("#add-device-popup ~ #overlay").click(function(){
        alert()
        $(".new-device-popup").addClass("hideMe");
    })
})

function clearInfo(){
    /* clears all inputs of Collector info section */
    $("input").val("")
}
