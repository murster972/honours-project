var visualizations;
var flows, temp;

$(document).ready(function(){
    let thresholds = new Thresholds();
    visualizations = new Visualizations();


    //visualizations.bytes(temp, "src");

    $("#mode").change(function(){
        let mode = $(this).val();

        getFlows();

        let v = new Visualizations(mode, flows);
    })

    $(".more-info").click(function(){
        clear_threshold_inputs();
        show_popup();
        let id = $(this).attr("id");

        thresholds.selected_id = id;

        thresholds.load_info(id);
    })

    $("#delete").click(function(){
        thresholds.delete_threshold()
    })

    $("#apply").click(function(){
        thresholds.create_threshold();
    })

    $("#add-threshold").click(function(){
        clear_threshold_inputs()
        show_popup()
        alert("Insert the threshold information in the fields below, leave a field blank if it's not part of the threshold")
    })

    $("#hide-popup").click(function(){
        $("#overlay").addClass("hideMe")
        $("#add-threshold-popup").addClass("hideMe")
    })
})

function show_popup(){
    $("#overlay").removeClass("hideMe")
    $("#add-threshold-popup").removeClass("hideMe")
}

function clear_threshold_inputs(){
    $("#add-threshold-popup #content input").val("")
}

function getFlows(){
    $.get("/get_flows", function(data){
        flows = JSON.parse(data);

        console.log(flows);
    })
}
