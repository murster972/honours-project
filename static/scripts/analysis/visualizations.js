
class Visualizations {
    constructor(mode, data){
        let modes = {"1": function(t, d){t.ip(d, "src")},
                     "2": function(t, d){t.ip(d, "dest")},
                     "3": function(t, d){t.ports(d, "src")},
                     "4": function(t, d){t.ports(d, "dest")},
                     "5": function(t, d){t.bytes(d, "src")},
                     "6": function(t, d){t.bytes(d, "dest")}}

        if(mode in modes){
            modes[mode](this, data);
        }
    }

    /*
     * shows average bytes per ip
     *
     * @param {list} flow_data, list of flows
     * @param {str} type, src or dest */
    bytes(flow_data, type){
        let ip_bytes = {};
        let byte_freq = this.freq(flow_data, "bytes");

        for(let i = 0; i < flow_data.length; i++){
            let f = flow_data[i];
            let ip = f[type + "-ip"];
            let bytes = f["bytes"];

            if(ip in ip_bytes){
                ip_bytes[ip].push(bytes);
            } else{
                ip_bytes[ip] = [bytes];
            }
        }

        let dataPoints = [];
        let keys = Object.keys(ip_bytes);

        for(let i = 0; i < keys.length; i++){
            let k = keys[i]
            let bytes = ip_bytes[k];
            let total = 0;

            for(let j = 0; j < bytes.length; j++){
                total += bytes[j];
            }

            dataPoints.push({y: total / bytes.length, label: k})
        }

        this.graph = new CanvasJS.Chart("visualization-area", {
            animationEnabled: true,
            title: {
                text: "Average Bytes for " + type + "-IP"
            }, legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "left",
                dockInsidePlotArea: false,
            }, axisX: {
                title: type + "-ip"
            }, axisY: {
                title: "Average bytes"
            }, data: [{
                type: "bar",
                showInLegend: true,
                dataPoints: dataPoints
            }]
        })

        this.graph.render();
    }

    /*
     * shows freq of ports as a pie chart
     *
     * @param {list} flow_data, list of flows
     * @param {str} type, src or dest port*/
    ip(flow_data, type){
        let ip_freq = this.freq(flow_data, type + "-ip");

        this.pieChart(type + " IP", ip_freq, flow_data.length);
    }

    /*
     * shows freq of ports as a pie chart
     *
     * @param {list} flow_data, list of flows
     * @param {str} type, src or dest port*/
    ports(flow_data, type){
        let port_freq = this.freq(flow_data, type + "-port");

        this.pieChart(type + " Ports", port_freq, flow_data.length);
    }

    pieChart(title, data, total_count){
        // converts values to perctantage
        let dataPoints = [];
        let keys = Object.keys(data);

        for(let i = 0; i < keys.length; i++){
            let k = keys[i];
            let val = data[k] / total_count * 100;

            dataPoints.push({y: val, name: k});
        }

        this.graph = new CanvasJS.Chart("visualization-area", {
            animationEnabled: true,
            title: {
                text: "Top " + title
            }, legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "left",
                dockInsidePlotArea: false,
            },
            data: [{
                type: "pie",
                toolTipContent: "{name}: <strong>{y}%</strong>",
		        indexLabel: "{name} - {y}%",
                showInLegend: true,
                dataPoints: dataPoints
            }]
        })

        this.graph.render();
    }

    /*
     * gets freqs of a value e.g. port or ip
     *
     * @param {list} data, list of flows
     * @param {key_} str, value for freq e.g. "src-port" */
    freq(data, key_){
        let freqs = {};

        for(let i = 0; i < data.length; i++){
            let tmp = data[i];
            let val = tmp[key_];


            if(val in freqs){
                freqs[val]++;
            } else{
                freqs[val] = 1;
            }
        }

        return freqs;
    }

}
