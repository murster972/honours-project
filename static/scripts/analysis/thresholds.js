
class Thresholds {
    constructor(){
        this.selected_id = "";
    }

    load_info(id){
        let index;
        // find info with same id
        for(index = 0; threshold_info[index]["_id"] != id; index++){}
        let info = threshold_info[index];

        $("[name='name']").val(info["name"][0]);
        $("[name='src-ip']").val(info["src-ip"][0]);
        $("[name='dest-ip']").val(info["dest-ip"][0]);
        $("[name='src-mac']").val(info["src-mac"][0]);
        $("[name='dest-mac']").val(info["dest-mac"][0]);
        $("[name='src-port']").val(info["src-port"][0]);
        $("[name='dest-port']").val(info["dest-port"][0]);
        $("[name='src-vlan']").val(info["src-vlan"][0]);
        $("[name='dest-vlan']").val(info["dest-vlan"][0]);
        $("[name='min-packet-count']").val(info["packet-count-min"][0]);
        $("[name='max-packet-count']").val(info["packet-count-max"][0]);
        $("[name='min-bytes']").val(info["bytes-min"][0]);
        $("[name='max-bytes']").val(info["bytes-max"][0]);
        $("[name='direction']").val(info["direction"][0]);
        $("[name='next-hop']").val(info["next-hop"][0]);
    }

    delete_threshold(){
        $.post("/delete_threshold", this.selected_id, function(){
            alert("Threshold deleted!");
        })
    }

    create_threshold(){
        // get values from fields
        let threshold_info = {}

        threshold_info["name"] = $("[name='name']").val();
        threshold_info["collectors"] = $("[name='collectors']").val();
        threshold_info["src-ip"] = $("[name='src-ip']").val();
        threshold_info["dest-ip"] = $("[name='dest-ip']").val();
        threshold_info["src-mac"] = $("[name='src-mac']").val();
        threshold_info["dest-mac"] = $("[name='dest-mac']").val();
        threshold_info["src-port"] = $("[name='src-port']").val();
        threshold_info["dest-port"] = $("[name='dest-port']").val();
        threshold_info["src-vlan"] = $("[name='src-vlan']").val();
        threshold_info["dest-vlan"] = $("[name='dest-vlan']").val();
        threshold_info["packet-count-min"] = $("[name='min-packet-count']").val();
        threshold_info["packet-count-max"] = $("[name='max-packet-count']").val();
        threshold_info["bytes-min"] = $("[name='min-bytes']").val();
        threshold_info["bytes-max"] = $("[name='max-bytes']").val();
        threshold_info["direction"] = $("[name='direction']").val();
        threshold_info["next-hop"] = $("[name='next-hop']").val();

        $.post("/create_threshold", threshold_info, function(data){
            alert("New Threshold created");
        })
    }

    delete_info(){

    }
}
