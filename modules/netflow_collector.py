#!/usr/bin/env python3
from threading import Thread, Lock
from pymongo import MongoClient
from random import getrandbits
import configparser
import datetime
import socket
import select
import time

import os

from netflow_parser import NetflowParser
from thresholds import Thresholds
import database_handler

" CHECK THIS FOR INFO: https://tools.ietf.org/html/rfc3954 , section called 'The Collector Side'"

#NOTE: class for collectors ???
#      they will be stored in a config file, but should they each have theyre own class
#      JSON or .ini ???
#      when to discard templates if not used ???
#      have eahc collector run its own instance of the parser OR run one instance and use it for every collector?

"""
TODO:
    - storage thread functionality
    - template_expire thread functionality
"""

class NetFlowCollector:
    ''' Listens for netflow on UDP port, parses and
        stores in JSON format '''

    CONFIG_LOCATION = "/home/muzza972/Documents/Programming/Honours Project/config/collectors.ini"
    INACTIVE_TIMER = 5

    #test id - 5ca894d3f29dd2f53038d05d

    def __init__(self):
        self.collector_info = {}
        self.collector_socks = []
        self.templates = {}

        # if flag is set will copy bytes to files instead of db, and not attempt to parse them
        # used to get bytes for testing away from lab
        self.testing = False

        if self.testing:
            print("[*] Testing enabled, will capture raw bytes instead of parsing.")
            self.export_count = 0
            cur_time = time.time()
            self.testing_dir = "../tests/NetFlow-Export-raw-bytes/{}/".format(cur_time)
            os.mkdir(self.testing_dir)

        # init db handler
        self.db_handler = database_handler.Collectors()

        # data flowsets to be put into storage
        self.storage_buffer = []

        # setup collecotrs (sockets, info, etc.)
        info = self.__get_collector_info()
        self.__create_collectors(info)

        # start storage thread
        storage = Thread(target=self.storage, args=[], daemon=True)
        storage.start()

        # start check_collectors thread
        updater = Thread(target=self.__collector_updates, args=[], daemon=True)
        updater.start()

        flow_rate = Thread(target=self.__update_incoming_rate, args=[], daemon=True)
        flow_rate.start()

        # status thread
        status = Thread(target=self.__update_status, args=[], daemon=True)
        status.start()

        # start template_time thread
        pass

        self.listen()

    def listen(self):
        ''' listens for incoming traffic on socket '''
        try:
            while True:
                # updates socks and sock_name, so new/deleted socks are detected
                socks = [i[1] for i in self.collector_socks]
                sock_name = {i[1]:i[0] for i in self.collector_socks}

                recv, write, e = select.select(socks, [], [], 0)

                # loops through sockets that have data waiting to be recv
                for sock in recv:
                    name = sock_name[sock]
                    export_record = sock.recv(100000)

                    # updates collectors recv, so wont be set as inactive
                    self.collector_info[name]["recv"] += 1
                    self.collector_info[name]["flow-rate"] += 1

                    #NOTE: create a seperate thread for handling each record ???]
                    self.handle_record(export_record, name)

                #time.sleep(1)

        except KeyboardInterrupt:
            # DONT EXPLICITLY SILENCE EXCPETIONS
            pass

    def handle_record(self, export_record, name):
        ''' deals with incoming traffic from NetFlow collectors,
            passes off to parser and stores if appropatiate '''
        if self.testing:
            self.__store_bytes(export_record, name)
            return 0

        data_flows, templates = self.__parser(export_record, name)

        #print("{}".format(str(data_flows)))

        # update templates
        self.templates[name] = templates

        # checks thresholds
        breaches = Thresholds(name, data_flows).check_flow()

        print(breaches)

        # add flows to storage
        for i in range(len(data_flows)):
            b = [] if not breaches else breaches[i]
            f = data_flows[i]
            f["time"] = time.time()
            self.storage_buffer.append((name, f, b))

        # add data_flows to storage
        #self.storage_buffer.append((name, data_flows))

    def __store_bytes(self, record, name):
        ''' stores export record as raw bytes in a file, used
          to get bytes for testing '''
        # store with export_count, so can be replayed in recieved order
        fname = "{} - {}".format(self.export_count, name)

        with open(self.testing_dir + fname, "wb") as f:
            f.write(record)

        self.export_count += 1
        print("saved record from - {} - to: {}".format(name, fname))

    def storage(self):
        ''' Uses DB handler to add parsed data flowsets to storage '''
        while True:
            if not self.storage_buffer:
                time.sleep(5)
                continue

            id, flow, breaches = self.storage_buffer[0][0], self.storage_buffer[0][1], self.storage_buffer[0][2]
            self.storage_buffer = self.storage_buffer[1:]

            self.db_handler.add_flows(id, flow)

            if breaches: self.db_handler.add_breach(id, breaches)

    def updater(self):
        ''' Uses DB handler to update collector info, e.g. status and no flows per second '''
        pass

    def __collector_updates(self):
        ''' checks for updates regarding collector deletion and insertion '''
        while True:
            # sleeps at start to avoid clash with listen() when first loaded
            time.sleep(5)

            c_info = self.__get_collector_info()
            c_ids = {i[0]:() + i for i in c_info}
            stored_ids = self.collector_info.keys()

            # gets ids in stored but not in updated ids, and deletes them
            to_delete = [id_ for id_ in stored_ids if id_ not in c_ids.keys()]
            if to_delete: self.__delete_collectors(to_delete)

            # gets ids in updated ids but not stored
            to_add = [c_ids[id_] for id_ in c_ids.keys() if id_ not in stored_ids]
            if to_add: self.__create_collectors(to_add)

    def __delete_collectors(self, ids):
        ''' called when collector has been removed, closes its socket and
            deletes its info in collector_info dict '''
        # remove from collector_info
        for id_ in ids: del self.collector_info[id_]

        # remove from collector_socks
        for ind in range(len(self.collector_socks) - 1, 0, -1):
            id_, sock = self.collector_socks[ind]

            if id_ in ids: del self.collector_socks[ind]

        print("[-] This collector has been removed: " + id_)

    def __template_timer(self):
        ''' Removes templates that have expired, see
            https://tools.ietf.org/html/rfc3954 '''
        pass

    def __create_collectors(self, collector_info):
        ''' creates new info for each collector and adds to current list of collectors '''
        # get config info for each collector
        #collector_info = self.__get_collector_info()

        for info in collector_info:
            name, ip_addr, port_no = info

            # adds collector to dict
            self.collector_info[name] = {"ip_addr": ip_addr, "port_no": port_no, "recv": 0, "flow-rate": 0, "status": "inactive"}

            # create socket for each collector
            sock = self.__create_socket(ip_addr, port_no)
            self.collector_socks.append((name, sock))

            # create template cache for each collector
            self.templates[name] = {}

            print("[+] Loaded new collector: " + name)

    def __update_incoming_rate(self):
        ''' Updated number of flows recv per second for a collector '''
        # NOTE: MOVE TO updatder method, only that both status updated and this
        #       are calling db
        while True:
            cur_time = time.time()
            for id_ in self.collector_info.keys():
                # TODO: change so db is only called once and not for each collector
                tmp = {"no": self.collector_info[id_]["flow-rate"], "time": cur_time}
                self.db_handler.update_collector(id_, {"flow-rate": tmp})

                self.collector_info[id_]["flow-rate"] = 0
            time.sleep(1)

    def __update_status(self):
        ''' keeps track of active/inactive collectors, where
            a collector is inactive if it hasnt recv any flows
            in the last N seconds - as defined in the clas variable
            INACTIVE_TIMER.'''
        # NOTE: MOVE TO updatder method, only that both status updated and this
        #       are calling db
        first_loop = True

        while True:
            for id_ in self.collector_info.keys():
                # only updates db if status has changed
                c = self.collector_info[id_]

                if first_loop or (c["recv"] == 0 and c["status"] == "active"):
                    self.collector_info[id_]["status"] = "inactive"
                    self.db_handler.update_collector(id_, {"status": "inactive"})
                    print("[*] status change for {}: {}".format(id_, "inactive"))

                elif c["recv"] > 0 and c["status"] == "inactive":
                    self.collector_info[id_]["status"] = "active"
                    self.db_handler.update_collector(id_, {"status": "active"})
                    print("[*] status change for {}: {}".format(id_, "active"))

                # NOTE: reset by thread updating incoming flow rate
                # reset recv counter
                self.collector_info[id_]["recv"] = 0

            first_loop = False
            time.sleep(NetFlowCollector.INACTIVE_TIMER)

    def __create_socket(self, ip_addr, port_no):
        ''' creates new socket '''
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((ip_addr, port_no))

        return sock

    def __get_collector_info(self):
        ''' gets information about all current collectors from db Collectors collection
            in config folder '''
        collectors = self.db_handler.get_collectors()
        config = []

        for collector in collectors:
            config.append((collector["_id"], collector["ip"], int(collector["port"])))

        return config

    def __parser(self, export, name):
        ''' converts netflow export to format that is human readable, returns
            any dataflow exports and any unused templates for future use

            :param export: bytes represnting netflow export
            :param templates: dictonary of templates '''
        templates = self.templates[name]
        parser = NetflowParser(export, templates)

        return parser.parse()

if __name__ == '__main__':
    NetFlowCollector()
