#!/usr/bin/env python3

def valid_ip(ip_addr):
    if not ip_addr: return False

    octets = ip_addr.split(".")

    if len(octets) != 4: return False

    for oct in octets:
        if not oct.isdigit() or (int(oct) < 0 or int(oct) > 255):
            return False

    return True

def valid_port(port):
    if not port.isdigit() or (int(port) < 0 or int(port) > 65535):
        return False

    return True


def valid_name(name):
    return False if not name or len(name.strip()) == 0 else True
