#!/usr/bin/env python3
def int_to_ip(val):
	''' turns int value into ip octets
	 	:param val: int, value of ip as int
		:output str: ip formated as octets'''
	expected_bits = 8 * 4

	# convert to bin, get padding len, add padding
	bin_ = to_bin(expected_bits, val)

	# seperate into octets - converts int to str so it can be joined
	octets = [str(int(bin_[i - 8:i], 2)) for i in range(8, len(bin_) + 1, 8)]

	return ".".join(octets)

def int_to_mac(val):
    ''' turns int value into mac addres
	 	:param val: int, value of mac as int
		:output str: formatted mac address'''
    #3c:95:09:9f:7e:b1
    # convert to bin, get padding len, add padding
    expected_bits = 8 * 6
    bin_ = to_bin(expected_bits, val)

    # seperate into four-bit blocks and convert to hex
    four_bits = [hex(int(bin_[i - 4:i], 2))[2:] for i in range(4, len(bin_) + 1, 4)]
    pairs = ["".join(four_bits[i - 2:i]) for i in range(2, len(four_bits) + 1, 2)]

    return ":".join(pairs)

def to_bin(expected_l, val):
    ''' converts int to bits
        :param expected_l: int, exp number of bits,
        :param val: int, value to convert,
        :output str: padded bin val '''
    # convert to bin, get padding len, add padding
    bin_ = bin(val)[2:]
    padd_l = expected_l - len(bin_)
    padd = "0" * padd_l

    return padd + bin_

if __name__ == '__main__':
    print(int_to_mac(66610809241265))
