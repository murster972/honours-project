#!/usr/bin/env python3
import sklearn

# NOTE: " Unknown applications can be handled by assigning their flows to unknown clusters"
#        give user the option to train the classifier for other types of applications ???
#        or allow the user to tell the classifier which applications will occur and to alert
#        if the ammount of unknown flows reaches N over a given time period?
#        allow user to visualize the k-means clusters and assign labels to unknown categories ???
#        have N predefine traffic classes but allow user to retrain for more ???
#        allow user to set alerts for specific unknown clusters or assing them label ???

# TODO: visualize data using graph to check if results are accurate or not

# NOTE: fields that will be required for netflow to collect:
#       src IPv4, dest IPv4, src Port, dst Port
#           ^ used for label propegation
#       No. of packets (field 1/24) (bi-dir),
#       vol. of bytes (field 2/23) (bi-dir),
#       min (field 25)/max (field 26)/std of packet size (bi-dir),
#       min/max/std of inter-packet time (bi-dir)
#           ^ used for k-means
#       seems netflow doesnt collect these MOTHER-FUCKING stats so new idea is just to
#       essentially use every field avaible and see if that works.

# TODO: change so that shared paramaters (e.g. extended_flowset) are self. vars instead
#       of having to be passed for each method

# TODO: finish compund classifier
# TODO: train and test

class FlowBasedCompoundClassifier:
    ''' An implementation of the flow-based compound-classification method with unknown flow
        detection as proposed in:
            Jun Zhang, A. V. et al. (2013) "An Effective Network Traffic
            Classification Method with Unknown Flow Detection". Network and
            Service Management, IEEE Transactions on. [Online] 10 (2), 133–147. '''

    def __init__(self):
        pass

    def test(self, flows):
        pass

    def train(self, flows):
        pass

    def compound_classifier(self, flows):
        ''' compound classifier using k-means algorthim '''
        # construct bag-of-flows based on 3-tuples (dest ip/port and trans protocol)
        bag_of_flows = []

        # use NCC classifier and majority vote rule to classify the
        # flows inside the bag of flows
        for bof in bag_of_flows:
            # predicts cluster using ncc for each flow in bof
            flow_class = [self.ncc_classifier.predict(flow) for flow in bof]

            #


    def nearest_cluster_classifier(self, exntedend_flows, cluster_no):
        ''' nearest cluster classifier using k-means and probabslistic scheme to assign
            clusters to an application-based class '''
        # create clusters and calc centroid of each using extended_flowset
        k_means = self._k_means(extended_flowset, cluster_no)
        clusters =

        # uses probablistic scheme to assign each cluster to a traffic class
        # or unknown class
        traffic_classes = extended_flowset.keys()
        class_clusters = {}

        for cluster in clusters:
            c_flows = cluster["flows"]
            # loop through each class and calcs prob cluster belongs to it
            max_prob = [-1, "Unknown"]
            for class_ in traffic_classes:
                count = sum([1 for flow in c_flows if flow in traffic_classes[class_]])
                prob = count / len(c_flows)

                if prob > max_prob[0]: max_prob = [prob, class_]

            # assigns cluster to class with highest prob, or unknown if it contains
            # no labelled flows
            class_cluster[max_prob[1]] = cluster

        # create NCC classifier user k-means clusters
        self.ncc_classifier = k_means
        self.cluster_classes = class_cluster

    def label_propegation(self, labelled_flowset, unlaballed_flowset):
        ''' ...
            :param labelled_flowset: dict, small set of labelled flows
            :param unlaballed_flowset: list, large set of unlabelled flows
            :param extended_flowset: dict, extened set of labelled flows '''
        extended_flowset = {label: labelled_flowset[i] for i in labelled_flowset.keys()}

        for label in labelled_flowset:
            for l_flow in label:
                for ul_flow in unlaballed_flowset:
                    if l_flow.IP_DST == ul_flow.IP_DST and l_flow.PORT_DST == ul_flow.PORT_DST \
                       and l_flow.TRANS_PROT == ul_flow.TRANS_PROT:
                       # since unlabelled flow shares same dest ip/port and transport protocol
                       # as labelled, its likely to be the same label, hence its assigned as such
                       extended_flowset[label].append(ul_flow)

        return extended_flowset

    def _k_means(self, extended_flowset, cluster_no):
        ''' k-means algorthim, using the scikit-learn library '''
        pass

if __name__ == '__main__':
    pass
