#!/usr/bin/env python3

' Dictionary of fields for netlow '

NETFLOW_FIELDS = {
    1: {"name": "IN_BYTES", "descr": "", "size": 0},
    2: {"name": "IN_PKTS", "descr": "", "size": 0},
    3: {"name": "FLOWS", "descr": "", "size": 0},
    4: {"name": "PROTOCL", "descr": "", "size": 0},
    5: {"name": "", "descr": "", "size": 0},
    6: {"name": "", "descr": "", "size": 0},
    7: {"name": "", "descr": "", "size": 0},
    8: {"name": "", "descr": "", "size": 0},
    9: {"name": "", "descr": "", "size": 0},
    10: {"name": "", "descr": "", "size": 0},
    11: {"name": "", "descr": "", "size": 0},
    12: {"name": "", "descr": "", "size": 0},
    13: {"name": "", "descr": "", "size": 0},
    14: {"name": "", "descr": "", "size": 0},
    15: {"name": "", "descr": "", "size": 0},
    16: {"name": "", "descr": "", "size": 0},
    17: {"name": "", "descr": "", "size": 0},
    18: {"name": "", "descr": "", "size": 0},
    19: {"name": "", "descr": "", "size": 0},
    20: {"name": "", "descr": "", "size": 0},
    21: {"name": "", "descr": "", "size": 0},
    22: {"name": "", "descr": "", "size": 0},
    23: {"name": "", "descr": "", "size": 0},
    24: {"name": "", "descr": "", "size": 0},
    25: {"name": "", "descr": "", "size": 0},
}

values = """IN_BYTES 1 N (default is 4) Incoming counter with length N x 8 bits for number of bytes associated with an IP Flow.
IN_PKTS 2 N (default is 4) Incoming counter with length N x 8 bits for the number of packets associated with an IP Flow
FLOWS 3 N Number of flows that were aggregated; default for N is 4
PROTOCOL 4 1 IP protocol byte
SRC_TOS 5 1 Type of Service byte setting when entering incoming interface
TCP_FLAGS 6 1 Cumulative of all the TCP flags seen for this flow
L4_SRC_PORT 7 2 TCP/UDP source port number i.e.: FTP, Telnet, or equivalent
IPV4_SRC_ADDR 8 4 IPv4 source address
SRC_MASK 9 1 The number of contiguous bits in the source address subnet mask i.e.: the submask in slash notation
INPUT_SNMP 10 N Input interface index; default for N is 2 but higher values could be used
L4_DST_PORT 11 2 TCP/UDP destination port number i.e.: FTP, Telnet, or equivalent
IPV4_DST_ADDR 12 4 IPv4 destination address
DST_MASK 13 1 The number of contiguous bits in the destination address subnet mask i.e.: the submask in slash notation
OUTPUT_SNMP 14 N Output interface index; default for N is 2 but higher values could be used
IPV4_NEXT_HOP 15 4 IPv4 address of next-hop router
SRC_AS 16 N (default is 2) Source BGP autonomous system number where N could be 2 or 4
DST_AS 17 N (default is 2) Destination BGP autonomous system number where N could be 2 or 4
BGP_IPV4_NEXT_HOP 18 4 Next-hop router's IP in the BGP domain
MUL_DST_PKTS 19 N (default is 4) IP multicast outgoing packet counter with length N x 8 bits
"""

temp = values.split("\n")

for i in range(1, 20):
    temp[i]
