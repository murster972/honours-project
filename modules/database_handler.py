#!/usr/bin/env python3
import configparser
from pymongo import MongoClient
from bson import ObjectId

# NOTE: using nosql instead (mongoDB), as it will, hopefully, be faster when querying (and wont require joins),
#		and from current experience its much nicer to use than mysql db and mysql python module

class Handler:

	''' Provides basic interaction with the DB - gives a layer of abstraction so other
	  	parts of the program don't have to worry about low-level programming to interact with the DB '''

	CONFIG_LOCATION = "/home/muzza972/Documents/Programming/Honours Project/config/database.ini"

	def __init__(self):
		# init connection with the db
		self.connection = MongoClient()
		self.db = self.connection["NetFlowClassification"]

	def query(self, collect, filter=None, find_one=True):
		''' finds value/s from documents in a colletion that match the filter
		 	:param collect: str, name of the collection
			:param filter: dict, filter for collection in dict format {key_value:value_to_filter}
			:param find_one: bool, option to return one or multiple matches
			:return: dict, found document '''
		collection = self.db[collect]

		if find_one:
			return collection.find_one(filter)
		else:
			return collection.find()

	def insert(self, collect, document, many=False):
		''' inserts document/s into a collection

			:return int: ID of inserted document/s '''
		collection = self.db[collect]

		if many:
			ids = collection.insert_many(document).inserted_ids
			return ids
		else:
			id = collection.insert_one(document).inserted_id
			return id

	def update(self, collect, new_values, filter):
		''' update values in collection

			:param collect: str, collection name
			:param new_values: dict, new values in dict format {key_value:new_values}
			:param filter: dict, filter for collection in dict format {key_value:value_to_filter} '''
		collection = self.db[collect]
		collection.update_one(filter, {"$set": new_values})

	def get_collection(self, collect):
		return self.db[collect]

	def remove(self, collect, filter):
		''' removes value from collection, using filter
		 	:param collect: str, collection name
			:param filter: dict, filter to match format {key_value:value_to_filter}
			:return int: number of deleted documents '''
		return self.db[collect].delete_one(filter).deleted_count

	def __get_config(self):
		' Returns the config info, store in database.ini, for connecting to the DB '
		pass

def remove_object_id(id):
	' turns ObjectId(id) into "id" '
	# so by default the _id value is stored as ObjectId(id) instead o fjust ID,
	# but ObjectID returns the id as str when returned, so is used
	# to get the return value from the objectID
	return str(id)

class Collectors:
	''' DB functions required for getting/updating collector information,
	 	provides another level fo abstraction on-top of Handler class, e.g.
		can simply call Collectors().get_collectors() to get details for all collectors,
		or Collectors().add_collector(values) to create a new collector'''

	def __init__(self):
		self.handler = Handler()

	def get_collectors(self, collector_id=None):
		''' gets collector/s info '''
		if not collector_id:
			cursor = self.handler.query("Collectors", find_one=False)
			collectors = []

			for collector in cursor:
				collectors.append(collector)
				c = collectors[-1]
				c["_id"] = remove_object_id(collectors[-1]["_id"])

				for i in range(len(c["flows"])):
					c["flows"][i] = remove_object_id(c["flows"][i])

			return collectors
		else:
			collector = self.handler.query("Collectors", filter={"_id": ObjectId(collector_id)})
			collector["_id"] = remove_object_id(collector["_id"])

			# remove objectID from flows
			for i in range(len(collector["flows"])):
				collector["flows"][i] = remove_object_id(collector["flows"][i])

			return collector

	def add_flows(self, collector_id, flow_data):
		''' Add flow to DB
			:param collector_id: str, id of collector
			:param flow_data: list, new flows '''
		# add to netflow collection
		document = {"collector": collector_id, **flow_data}


		inserted_id = self.handler.insert("NetflowData", document)


		# add to collector collection
		collector = self.handler.query("Collectors", filter={"_id": ObjectId(collector_id)})
		collector_flows = collector["flows"]

		collector_flows.append(inserted_id)
		self.handler.update("Collectors", {"flows": collector_flows}, {"_id": ObjectId(collector_id)})

		# adds breaches the flow has broken
		# collector = self.handler.query("Collectors", filter={"_id": ObjectId(collector_id)})
		# collector_breaches = collector["breaches"]
		# collector_breaches += breaches

		# self.handler.update("Collectors", {"breaches": collector_breaches}, {"_id": ObjectId(collector_id)})

	def add_breach(self, id_, breaches):
		''' Adds breach to collector '''
		collector = self.handler.query("Collectors", filter={"_id": ObjectId(id_)})
		collector_breaches = collector["breaches"]

		collector_breaches += breaches

		self.handler.update("Collectors", {"breaches": collector_breaches}, {"_id": ObjectId(id_)})
		

	def add_collector(self, data):
		''' add new collector to collectors collection
		 	:param data: dict, format {name: "", ip: "", port: ""}:
			:return tuple: (status, msg), status is 1 if invalid data, 0 if valid, msg is used to show
			 			   what data was invalid e.g. (1, "port") or the ID of the created collector '''
		# check if collector name or port exists first
		current = self.get_collectors()

		matches = []
		for key in ["name", "port"]:
			match = any(d[key] == data[key] for d in current)

			if match: matches.append(key)
		if matches: return ("2", ", ".join(matches))

		# doesnt exsits so create collector
		other_data = {"status": 1, "flows": [], "flow-rate": {"no": 0, "time": ""}, "activity_log": [], "breaches": []}
		new_collector = {**data, **other_data}
		inserted_id = self.handler.insert("Collectors", new_collector)

		return ("0", remove_object_id(inserted_id))

	def remove_collector(self, collector_id, remove_flows=False):
		''' removes collector from DB '''
		return self.handler.remove("Collectors", {"_id": ObjectId(collector_id)})

	def update_collector(self, collector_id, values):
		''' Update values for a collector '''
		self.handler.update("Collectors", values, {"_id": ObjectId(collector_id)})

class Thresholds:
	def __init__(self):
		self.handler = Handler()

	def add_threshold(self, data):
		#fields = []
		inserted_id = self.handler.insert("Thresholds", data)

		return (0, inserted_id)

	def delete_threshold(self, threshold_id):
		return self.handler.remove("Thresholds", {"_id": ObjectId(threshold_id)})

	def get_thresholds(self):
		cursor = self.handler.query("Thresholds", find_one=False)
		thresholds = []

		for threshold in cursor:
			thresholds.append(threshold)
			t = thresholds[-1]
			t["_id"] = remove_object_id(thresholds[-1]["_id"])

		return thresholds

	def delete_threshold(self, threshold_id):
		''' removes collector from DB '''
		return self.handler.remove("Thresholds", {"_id": ObjectId(threshold_id)})

class Flows:
	def __init__(self):
		self.handler = Handler()

	''' get all flows '''
	def get_flows(self):
		cursor = self.handler.query("NetflowData", find_one=False)
		flows = []

		for f in cursor:
			flows.append(f)
			flows[-1]["_id"] = remove_object_id(flows[-1]["_id"])

		return flows
