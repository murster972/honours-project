#!/usr/bin/env python3
from telegram.ext import Updater, CommandHandler
from threading import Thread
import urllib.parse
import subprocess
import time
import os

from commands import BotCommands

''' telegram bot used to send notifcations regarding threshold breaches from web-app to 
	users phone '''

# https://www.shellhacks.com/telegram-api-send-message-personal-notification-bot/

# https://api.telegram.org/bot<TOKEN>/sendMessage?chat_id=<CHAT_ID>&text=Hello%20World

class BlackBeard:
	' Represents an instance of a telegram BlackBeard telegram bot '
	# time to rest in-between checking for new commands
	CMD_PERIOD = 3

	def __init__(self, token, chat_id):
		self.token = token
		self.chat_id = chat_id

		self.base_url = "https://api.telegram.org/bot{}/".format(self.token)

		# commands (and their functions) currently supported by BlackBeard
		self.commands = BotCommands()

		self.handle_commands()

	def send_msg(self, msg):
		''' send message from bot to chat with passed id '''
		url = "{}sendMessage".format(self.base_url)

		m = urllib.parse.quote(msg, safe="")

		cmd = "curl -s -X POST {} -d chat_id={} -d text={}".format(url, self.chat_id, m)

		# replace true and false so can use eval to turn into dict object
		res = self._pythonic_bool(self._run_cmd(cmd))
		res = eval(res)

		if res["ok"]:
			print("[🡒] Sent the following msg to chat [{}]: {}".format(self.chat_id, msg))
		else:
			print("[!] Failed to send the msg to chat, see error as follows: {}".format(res))

	def handle_commands(self):
		''' checks if bot has recv any new commands every N seconds. if recv command
			is valid will carry out, else will return invalid command msg '''
		while True:
			cur_time = time.time()
			time.sleep(BlackBeard.CMD_PERIOD)

			cmds = self.get_recv_cmds(chat_id=self.chat_id)

			# filter for commmands recv after current time
			new_commands = [cmd for cmd in cmds if cmd["time"] >= cur_time]

			if len(new_commands) == 0:
				print("[*] No new commmands")
				continue

			for cmd in new_commands:
				print(cmd)
				# attempt to run command
				res = self.commands.run(cmd["cmd"], cmd["args"])

				self.send_msg(res)

	def get_recv_cmds(self, chat_id="", last=True):
		''' returns a list of cmds recv by the bot
			:param chat_id: if passed, filter for specific chat
			:param last: return all or last recv, true by default '''
		updates = self._bot_updates()["result"]
		cmds = []

		id_ = chat_id if not chat_id else int(chat_id)

		for recv in updates:
			if "message" in recv:
				msg = recv["message"]
			else:
				msg = recv["edited_message"]

			# skip non commands
			if "entities" not in msg or msg["entities"][0]["type"] != "bot_command": continue

			# filter if chat_id passe td
			if chat_id and msg["chat"]["id"] != id_: continue


			# NOTE: [1:] strips the '/' from start of command
			cmd = msg["text"].split(" ")

			info = {"cmd": cmd[0][1:], "args": cmd[1:], "message_id": msg["message_id"], "chat_id": msg["chat"]["id"], "time": msg["date"]}

			cmds.append(info)

		return cmds
		
	def _bot_updates(self):
		''' gets all update message for the bot '''
		url = "{}getUpdates".format(self.base_url)

		cmd = "curl -s -X GET {}".format(url)

		return eval(self._pythonic_bool(self._run_cmd(cmd)))

	def _run_cmd(self, cmd):
		res = subprocess.run(cmd.split(), stdout=subprocess.PIPE)
		return res.stdout.decode()

	def _pythonic_bool(self, s):
		''' replace true/false with True/False '''
		return s.replace("true", "True").replace("false", "False")

if __name__ == '__main__':
	token = "811137737:AAFtQKhoP8IXyFX_7zC6s4NXHd9kFGEmWJM"
	chat_id = "839834310"

	bot = BlackBeard(token, chat_id)

	d = bot.get_recv_cmds(chat_id=chat_id)

	#for cmd in d: print(cmd)