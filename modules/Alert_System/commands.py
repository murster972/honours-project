#!/usr/bin/env python3
import sys
import bson

# TODO: change this as it's HACKY AF
sys.path.append("../")

import database_handler

'TODO: add commands for adding/deleting collectors'

class BotCommands:
	''' Commands for BlackBeard bot; verifys, checks, runs and returns output
		of command '''

	def __init__(self):
		self.commands = {"help": lambda x: self._help(),
						 "view_collectors": lambda x: self._view_collectors(),
						 "create_threshold": lambda info: self.create_threshold(info), 
						 "delete_threshold": lambda info: self._delete_threshold(info[0]), 
						 "view_thresholds": lambda ID: self._view_thresholds(ID)}

		self.thresholds = database_handler.Thresholds()
		self.collectors = database_handler.Collectors()

	def run(self, cmd_name, args):
		''' runs command
			:param cmd_name: name of command
			:param args: arguments to pass to cmd '''

		print(cmd_name, args)

		if not self._check_command(cmd_name):
			return "[!] The sent command '{}' is not valid".format(cmd_name)
		else:
			return self.commands[cmd_name](args)

	def _check_command(self, name):
		''' checks if a command exsists 
			:param name: name of commmand to run '''
		return name in self.commands

	def _help(self):
		msg = "Welcome To Blackbeard!, this bot allows you to communicate with [name] to: \
			   create, delete and view thresholds.\nThe following commands are currently avaiable:\
			   	\n· /help - send back list of commands and their description\
				\n· /view_collectors - send back list of NetFlow collectors (their name, ip, and port)\
				\n· /create_threshold - allow user to create new threshold\
				\n· /delete_threshold - allow user to delete threshold\
				\n· /view_thresholds  - send back list of all breached thresholds"

		return msg

	def _view_collectors(self):
		''' return list of all collectors with their name, ip and port '''
		collectors = self.collectors.get_collectors()

		if not collectors: return "[!] There are no collectors currently configured..."

		# only include relevant info for each collector
		rel_keys = ["_id", "name", "ip", "port", "status"]
		to_send = []

		for c in collectors:
			info = ["{} = {}".format(key, c[key]) for key in c if key in rel_keys]
			to_send.append("\n".join(info))

		to_send = "\n\n".join(to_send)

		return "The following collectors are avaiable: \n{}".format(to_send)

	def _create_threshold(self, info):
		return "command not avaiable yet..."

		# list of valid variables and there types and if static or range
		variables = {"name": {"mode": "static", "type_": type("str"), "required": True},
					 "collector_id": {"mode": "static", "type_": type("str"), "required": True},
					 "src_port": {"mode": "static", "type_": type(0), "required": False},
					 "dst_port": {"mode": "static", "type_": type(0), "required": False},
					 "src_ip": {"mode": "static", "type_": type("str"), "required": False},
					 "dst_ip": {"mode": "static", "type_": type("str"), "required": False},
					 "src_mac": {"mode": "static", "type_": type("str"), "required": False},
					 "dst_mac": {"mode": "static", "type_": type("str"), "required": False},
					 "src_vlan": {"mode": "static", "type_": type(0), "required": False},
					 "dst_vlan": {"mode": "static", "type_": type(0), "required": False},
					 "bytes": {"mode": "range", "type_": type(0), "required": False},
					 "packet_count": {"mode": "range", "type_": type(0), "required": False}}

		if len(info) == 1 and (info[0] == "-help" or info[0] == "--help"):
			# send back info regrading format of command
			# format is to specify by variables e.g. name=etst_1,src_port=10 etc.
			# with NO spaces inbetween, name is required and at least one other
			format_ = "/create_threshold "
			return "To create a threshold use the following format: \n"

		#threshold_info = {"name": }

		# parse input variables
		arguments = info[0].split(",")

	def _delete_threshold(self, id_):
		''' delte thresholds with current ID '''
		valid = "[-] The threshold with ID {} has been deleted".format(id_)
		invalid = "[!] Unable to delete threshold with ID {}, please check it is a valid ID".format(id_)

		try:
			res = self.thresholds.delete_threshold(id_)
		except bson.errors.InvalidId:
			return invalid

		return valid if res else invalid

	def _view_thresholds(self, ID=""):
		''' gets current thresholds '''
		thresholds = self.thresholds.get_thresholds()


		return "Following thresholds are currently active: \n{}".format(thresholds)

if __name__ == '__main__':
	b = BotCommands()

	c = b._view_collectors()