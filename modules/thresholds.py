#!/usr/bin/env python3
from threading import Thread, Lock
import time

import database_handler

class Thresholds:
    def __init__(self, collector_id, flows):
        self.db_handler = database_handler.Thresholds()

        self.collector_id = collector_id
        self.flows = flows

        self.get_thresholds()

    def check_flow(self):
        ''' compares export_record against current thresholds, checks if any of the flows
            in the export_record breach any thresholds '''

        if not self.thresholds: return []

        breached_flows = []

        for flow in self.flows:
            # thresholds the flow has breached
            # breaches = [(threshold_id, [breached_values]), ...]
            breaches = []

            for t in self.thresholds:
                # skip threshold if collector is not part of it
                if self.collector_id not in t["collectors"]:
                    #print("[*] collector not part of threshold")
                    continue


                range_vals = ["bytes-min", "bytes-max", "packet-count-min", "packet-count-max"]
                forget_vals = ["_id", "name", "collectors"]

                # checks if threshold mathces abs values
                match = 1
                breached = []

                # goes through threshold values
                for val in t.keys():
                    # skip blank values
                    if t[val][0] == "": continue

                    if val in range_vals:
                        # remove min/max from val
                        flow_val = "-".join(val.split("-")[:-1])

                        # breached if flow val is below or above min/max
                        if flow_val in flow and (("min" in val and flow[flow_val] < int(t[val][0])) or ("max" in val and flow[flow_val] > int(t[val][0]))):
                            breached.append(val)

                    elif val not in forget_vals:
                        # checks abs val, if flow val doesnt match then threshold doesnt apply
                        # i.e. its not a match
                        # skips values that are blank or not in flow fields

                        if val not in flow or str(flow[val]) != t[val][0]:
                            match = 0
                            break

                # flow breached current threshold
                if match and breached:
                    print("[!] Threshold Breach: {} - {}".format(t["_id"], str(breached)))
                    b = {"threshold_id": t["_id"], "breached_vals": breached, "time": time.time()}
                    breaches.append(b)

            breached_flows.append(breaches)

        return breached_flows

    def get_thresholds(self):
        ''' gets thresholds from DB '''
        self.thresholds = self.db_handler.get_thresholds()
