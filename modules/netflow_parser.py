#!/usr/bin/env python3
import struct

import socket

from misc.from_int import int_to_ip, int_to_mac

# NOTE: struct.unpack() uses "!" in format for network(=big-endian)
#		! converts little-to-big if needed, and does nothing if bytes
#		are already bug-endian
#		e.g. two-byte value b34f would be stored as two bytes b4 followed by 4f,
#			 the "big-end" is stored first

# https://www.cisco.com/en/US/technologies/tk648/tk362/technologies_white_paper09186a00800a3db9.pdf
# https://tools.ietf.org/html/rfc3954


"""
TODO:
	- create dict of field values
	- add support for options tempalte/data flowsets
"""


class NetflowParser:
	HEADER_SIZE = 20
	FLOWSET_ID = 2

	FIELDS = {1: {"name": "bytes", "funct": lambda x: x},
			  2: {"name": "packet-count", "funct": lambda x: x},
			  61: {"name": "direction", "funct": lambda x: x},
			  15: {"name": "next-hop", "funct": lambda x: x},
			  8: {"name": "src-ip", "funct": lambda x: int_to_ip(x)},
			  12: {"name": "dest-ip", "funct": lambda x: int_to_ip(x)},
			  7: {"name": "src-port", "funct": lambda x: x},
			  11: {"name": "dest-port", "funct": lambda x: x},
			  56: {"name": "src-mac", "funct": lambda x: int_to_mac(x)},
			  57: {"name": "dest-mac", "funct": lambda x: int_to_mac(x)},
			  58: {"name": "src-vlan", "funct": lambda x: x},
			  59: {"name": "dest-vlan", "funct": lambda x: x}}

	' converts bytes from netflow export to readable data '
	def __init__(self, record, templates={}):
		self.record = record
		self.templates = templates
		self.flows = []

		self.buffer = []

	def parse(self):
		''' extarcts template and data flowsets from bytes represnting the netflow
			export record

			:return tuple: (data flowsets, unused templates) '''
		header = self.__extract_header()

		# loop through remainning bytes extracting template and data flowsets
		while self.record:
			flowset_id = struct.unpack("!H", self.__record_bytes(2))[0]

			# skip options template/data flowsets, to be added in future
			if flowset_id == 1:
				self.__discard_options()
				continue

			if 0 <= flowset_id <= 255:
				self.__template_flowset(flowset_id)
			else:
				self.__data_flowset(flowset_id)

		# BUG: empty dict appearing at end of dataflowsets sometimes

		return (self.flows, self.templates)

	def __template_flowset(self, flowset_id):
		''' used when parsing template flowsets, extracts template info and adds
			to the template cache. '''
		# gets length, template id and field count
		flowset_info = struct.unpack("!HHH", self.__record_bytes(6))


		# gets all fields from template
		field_count = flowset_info[2]

		#BUG-FIX: info wasnt appearing correctly, forget to add "!" to format
		#		  so that the byte-order was correct

		template_id = flowset_info[1]
		self.templates[template_id] = []

		for i in range(field_count):
			bytes_ = self.__record_bytes(4)

			# BUG: sometimes doesnt have enough bytes, may be because theyre all zero, e.g.
			#	   there are four bytes but are b'\x00\x00\x00\x00'

			field_type, field_len = struct.unpack("!HH", bytes_)

			self.templates[template_id].append((field_type, field_len))

	def __data_flowset(self, flowset_id):
		''' used when parsing data flowsets, uses appropriate template to extract '''
		length = struct.unpack("!H", self.__record_bytes(2))[0]

		template_id = flowset_id

		try:
			flowset_fields = self.templates[template_id]

			del self.templates[template_id]

		except KeyError:
			# the data flowset is discarded if its template does not exist
			# in accordance with cisco whitepaper
			print("[ERROR] data flowset discarded due to non-existent template ID")
			return 0

		# calc number of records and padding (in bytes) at end of flowset
		field_bytes = sum([i[1] for i in flowset_fields])
		record_bytes = length - 2
		no_records = record_bytes // field_bytes

		#BUG: padding is to large, 5-bytes for example
		padding = record_bytes - (field_bytes * no_records)

		# loop through getting all records from dataflowset, using fields from template
		records = []

		for rec_no in range(no_records):
			# str value is use for key as its required by MongoDB
			# when instering a new document
			key = str(rec_no)

			records.append({})

			for field in flowset_fields:
				f_type, f_len = field

				bytes_ = self.__record_bytes(f_len)

				f_type = int(f_type)
				value = int(bytes_.hex(), 16)

				if f_type in NetflowParser.FIELDS:
					value = NetflowParser.FIELDS[f_type]["funct"](value)
					f_type = NetflowParser.FIELDS[f_type]["name"]

				#records[key].append((f_type, value))
				records[-1][str(f_type)] = value

		# removes padding from record
		self.__record_bytes(padding)

		self.flows += records

	def __extract_header(self):
		''' extracts header from export record bytes, and gets information relevant to
		  extracting data and template flowsets '''
		# header is 20bytes, with two 16-bit fields followed by four 32-bit fields
		# H - 2-bytes, I - 4-bytes
		header_bytes = self.__record_bytes(NetflowParser.HEADER_SIZE)
		header_fields = struct.unpack("!HHIIII", header_bytes)
		header_fields = list(map(hex, header_fields))

		return {"ver": header_fields[0], "count": header_fields[1], "ID": header_fields[5]}

	def __record_bytes(self, n):
		''' return first n-bytes of the record, and removes from record'''
		tmp = self.record[:n]
		self.record = self.record[n:]

		return tmp

	def __discard_options(self):
		''' discards of bytes used in options templates, is a temp method,
			and will be removed in future '''
		# bytes used by flowsetid and length fields
		cur_length = 4

		length = struct.unpack("!H", self.__record_bytes(2))[0] - cur_length

		# remove bytes from record
		self.__record_bytes(length)

if __name__ == '__main__':
	test_bytes = open("../tests/NetFlow-Export-raw-bytes/1554632850.0446706/0 - 5ca9cb4c36d11899c90bd558", "rb").read()

	x = NetflowParser(test_bytes)
	d = int_to_ip(3)

	print(d)
