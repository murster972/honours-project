#!/usr/env python3
from flask import Flask, render_template, request
import json

import back_end

app = Flask(__name__)

@app.route("/netflow_stats", methods=["GET"])
def netflow_stats():
    ' gets all stats and data for live graph '
    pass

@app.route("/netflow_collector", methods=["POST"])
def netflow_collector():
    collector_id = request.get_data().decode()

    info = back_end.get_collector_info(collector_id=collector_id)

    return json.dumps(info)

@app.route("/add_collector", methods=["POST"])
def add_collector():
    name = request.form.get("name")
    ip = request.form.get("ip")
    port = request.form.get("port")
    new_values = {"name": name, "ip": ip, "port": port}

    status, msg = back_end.add_collector(new_values)
    resp = {"status": status, "msg": msg}

    return json.dumps(resp)

@app.route("/delete_collector", methods=["POST"])
def delete_collector():
    collector_id = request.get_data().decode()

    # no collector selected
    if not collector_id:
        return "2"
    elif back_end.delete_collector(collector_id):
        return "1"
    else:
        return "3"

@app.route("/netflow")
def netflow():
    # get collector info
    info = back_end.get_collector_info(minimum=True)
    names = [i["name"] for i in info]
    ids = [i["id"] for i in info]

    return render_template("netflow.html", collector_info=info, collector_names=names, collector_ids=ids)

@app.route("/delete_threshold", methods=["POST"])
def delete_threshold():
    id = request.get_data().decode()

    back_end.delete_threshold(id)

    return "0"

@app.route("/create_threshold", methods=["POST"])
def create_threshold():
    data = request.form.to_dict(flat=False)

    return str(back_end.create_threshold(data))

@app.route("/get_flows", methods=["GET"])
def get_flows():
    data = back_end.get_flows()

    return json.dumps(data)

@app.route("/analysis")
def analysis():
    # get collector info
    t_info = back_end.get_thresholds()
    c_info = back_end.get_collector_info()

    return render_template("analysis.html", threshold_info=t_info, collectors=c_info)

@app.route("/")
def home_page():
    return render_template("index.html")

if __name__ == '__main__':
    app.run(debug=True, use_reloader=True)
