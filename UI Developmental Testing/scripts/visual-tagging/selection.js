/* selection system */
var dragged, startCoords, endCoords, boundingRect;

function mousePressed(){
    // move center of coords to start point
    boundingRect = {p1: createVector(mouseX, mouseY),
                    p2: createVector(0, 0),
                    p3: createVector(0, 0),
                    p4: createVector(0, 0)}
}

function mouseDragged(){
    dragged = true;
    boundingRect.p3 = createVector(mouseX, mouseY);
    calcBound();
}

function mouseReleased(){
    dragged = false;
}

function calcBound(){
    /* gets other two points that form bounding rect */
    boundingRect.p2 = createVector(boundingRect.p1.x, boundingRect.p3.y);
    boundingRect.p4 = createVector(boundingRect.p3.x, boundingRect.p1.y);
}
