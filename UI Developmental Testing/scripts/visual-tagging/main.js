
function setup(){
    createCanvas(800, 500);

    translate(width / 2, height / 2);

    strokeWeight(2);
    stroke(0);
    fill(0, 0, 0, 100);

    testData = {"1": 0, "2": 12, "3": 30, "4": 12, "10": 5, "6": 3}

    testData = {}

    for(let i = 0; i < 20; i++){
        testData[i] = Math.floor((Math.random() * 50) + 1);
    }

    //barChart(400, 400, testData);

    //noLoop();
}

function draw(){
    background("#fff");

    barChart({x: 20, y: height - 20}, 400, 300, testData);

    push();

    // selection rect
    if(dragged){
        beginShape();
        vertex(boundingRect.p1.x, boundingRect.p1.y)
        vertex(boundingRect.p2.x, boundingRect.p2.y)
        vertex(boundingRect.p3.x, boundingRect.p3.y)
        vertex(boundingRect.p4.x, boundingRect.p4.y)
        endShape(CLOSE);
    }

    pop();
}
